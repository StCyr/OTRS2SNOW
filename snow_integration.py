#!/usr/bin/python
import urllib,urllib2
import base64,json
import sys,time,getpass, ConfigParser
from pprint import pprint

# For OTRS web services (uses https://github.com/ewsterrenburg/python-otrs)
from otrs.ticket.template import GenericTicketConnectorSOAP
from otrs.client import GenericInterfaceClient
from otrs.ticket.objects import Ticket, Article, DynamicField, Attachment

class SnowClient:

    auth_token = ''

    # Generate an authentication token for the service-now instance
    def generate_authentication_token( self, username, passwd):
        self.auth_token = base64.encodestring('%s:%s' % (username, passwd)).replace('\n','')

    # Create an incident ticket 
    #   data: a dict containing the ticket information
    #   returns: ID of the SNOW ticket created (or nothing in case of error)
    def create_incident( self, data ):

        # Create request
        request = urllib2.Request('https://' + config.get('SNOW','fqhn') + '/api/now/v1/table/incident', data=json.dumps(data))

        # Add headers
        self.__addStandardHeaders(request)
        request.add_header('Content-Type', 'application/json')

        # Post request
        try:
            fh = urllib2.urlopen(request)
            resp = fh.read()
            json_resp = json.loads(resp)
            return json_resp['result']['sys_id']
        except Exception as error:
            ret = self.__handleError("Error when trying to create incident", error)
            if ret:
                self.create_incident(data)    
            else:
                pprint(data)
                return ''
  
    # Update an incident ticket
    #   id: the ID of the SNOW ticket
    #   data: a dict containing the update information
    #   returns: nothing
    def update_incident( self, id, data ):

        # Create request
        url = 'https://' + config.get('SNOW','fqhn') + '/api/now/v1/table/incident/' + str(id) + '?sysparm_exclude_ref_link=true'
        request = urllib2.Request(url, data=json.dumps(data))

        # Add headers
        self.__addStandardHeaders(request)
        request.add_header('Content-Type', 'application/json')

        # POST are not accepted
        request.get_method = lambda: 'PATCH'

        # Post request
        try: 
            fh = urllib2.urlopen(request)
            resp = fh.read()
        except Exception as error:
            ret = self.__handleError("Error when trying to update incident", error)
            if ret:
                self.update_incident(id, data)    
            else:
                pprint(data)
  
    # Adds an attachment to a ticket
    #   id: the ID of the SNOW ticket
    #   data: a dict containing the attachment information
    #   returns: nothing
    def add_attachment(self, id, data):
    
        # Snow doesn't accept 'application/pgp-signature' MIME type
        if (data['ContentType'].startswith('application/pgp-signature') or data['ContentType'].startswith('application/pgp-keys') or 
            data['ContentType'].startswith('application/octet-stream')  or data['ContentType'].startswith('text/plain')):
            print("Cannot add attachment of type '" + data['ContentType'] + "'")
            return

        contentType = data['ContentType'].partition(';')[0]
        # Verify mandatory parameters
        for k in ('Filename', 'ContentType', 'Content'):
            if k not in data:
                print("Error in function add_attachment: Missing mandatory parameter " + k)
                return 
  
        # Create request
        attachment = base64.b64decode(data['Content'])
        url = 'https://' + config.get('SNOW','fqhn') + '/api/now/attachment/file?table_name=incident&table_sys_id=' + str(id) + '&file_name=' + urllib.quote_plus(data['Filename'].encode('utf-8'))
        request = urllib2.Request(url, attachment)
  
        # Add headers
        self.__addStandardHeaders(request)
        request.add_header('Content-Type', contentType)
        request.add_header('Content-Length', len(attachment))
  
        # Post request
        try: 
            fh = urllib2.urlopen(request)
            resp = fh.read()
        except Exception as error:
            ret = self.__handleError("Error when trying to add an attachment", error)
            if ret:
                self.add_attachment(id, data)    
            else:
                pprint(data)
  
    # Handle communication error with the snow instance
    #  customReason: An application-specific error message
    #  error: The generic exception raised
    #  returns: True if calling method should be retried, False otherwise
    def __handleError(self, customReason, error):
        ret = False

        if error.code == 401:
            print("401 (Unauthorized) response received. Waiting 60 seconds before retrying...") 
            ret = True
            time.sleep(60)
        else:
            print(customReason)
            print(error)

        return ret

    # Add standard headers for the snow instance
    #  req: The Request object to which headers should be added
    #  returns: nothing
    def __addStandardHeaders(self, req):
        req.add_header('Authorization', 'Basic ' + self.auth_token)
        req.add_header('Accept', 'application/json')


################################################################################################
#
# Main program starts here
#
################################################################################################

config = ConfigParser.ConfigParser()
config.read('config_local.ini')

# Prompt user credentials
try:

    if config.get('SNOW','user') == '':
        username = raw_input("Please enter your service-now username: ")
    else:
        username = config.get('SNOW','user')

    if config.get('SNOW','passwd') == '':
        passwd = getpass.getpass()
    else:
        passwd = config.get('SNOW','passwd')

except Exception as error:

    print("ERROR", error)
    exit()

# Generate Snow authentication token
snow_client = SnowClient()
snow_client.generate_authentication_token(username, passwd)

# Open session with our ticketing system
client = GenericInterfaceClient('https://' + config.get('OTRS','fqhn'), tc=GenericTicketConnectorSOAP(config.get('OTRS','soap_endpoint')))

# Try getting results using existing session_id. Fallback to create a new session when it doesn't work.
# TODO: This code doesn't handle the case when SessionCreate fails
#for id in range(3,15000):
for id in range(1000,15000):

    # Get OTRS ticket
    try:
        session_file = open("/tmp/otrs_session","r")
        client.session_id = session_file.read()
        session_file.close()
        ticket = client.tc.TicketGet(id, get_articles=True, get_attachments=True)
    except:
        try:
            session_id = client.tc.SessionCreate(user_login=config.get('OTRS','user'), password=config.get('OTRS','passwd'))
            session_file = open("/tmp/otrs_session","w")
            session_file.write(session_id)
            session_file.close()
            ticket = client.tc.TicketGet(id, get_articles=True, get_attachments=True)
        except Exception as error:
            print("Did not found any OTRS ticket with ID = " + str(id))
            print("Error", error)
            continue

    # Skip merged OTRS tickets
    if ticket.StateType == 'merged':
        print("skipping ticket " + str(id) + " which is a merged ticket.")
        continue

    # Set incident data
    # Assigned to group "ICT-Logistics", caller "BELNET_LOUISE Contact", company "BELNET LOUISE"
    data = {
        'description': ticket.Title,
        'short_description': ticket.Title,
        'opened_at': ticket.Created,
        'sys_created_on': ticket.Created,
        'incident_state': '7',
        'assignment_group': config.get('SNOW','assignment_group'),
        'caller_id': config.get('SNOW','called_id'),
        'company': config.get('SNOW','company')
    }

    # Set incident state to "New", "In progress", or "Closed"
    if ticket.StateType == 'closed':
        data['closed_at'] = ticket.Changed,
        data['resolved_at'] = ticket.Changed,
        data['incident_state'] = '7',
    elif ticket.StateType == 'new':
        data['incident_state'] = '1',
    else:
        data['incident_state'] = '2',

    # Create incident
    ticket_id = snow_client.create_incident(data)

    # Skip to next OTRS ticket in case of error
    if ticket_id == '':
        continue

    print("Created SNOW ticket " + str(ticket_id) + " for OTRS ticket " + str(ticket.TicketNumber) + " (TicketID = " + str(id) + ")")

    # Create a worknote for each ticket article 
    for article in ticket.childs['Article']:

        # Get article's body
        try:
            data = { 'work_notes': article.Body }
        except TypeError:
            data = { 'work_notes': 'No body' }

        # Add worknote
        snow_client.update_incident(ticket_id, data)

        # Add attachments if any
        if 'Attachment' in article.childs:
            for attachment in article.childs['Attachment']:
                data = {
                    'Content': attachment.Content,
                    'ContentType': attachment.ContentType,
                    'Filename': attachment.Filename
                }
                snow_client.add_attachment(ticket_id, data)

    # Close incident
    data = { 'incident_state': '7' }
    snow_client.update_incident(ticket_id, data) 


####################################################################################################
#
# Retrieve existing incidents: Role required: admin, web_service_admin, or rest_api_explorer
# Create an incident record  : Role required: admin, web_service_admin, or rest_api_explorer
#
####################################################################################################
#
# Example of a SNow incident ticket
#
####################################################################################################
#{
#          "result": [
#                  {
#                            "additional_assignee_list": "",
#                            "cmdb_ci": {
#                                              "link": "https://belnetdev.service-now.com/api/now/v1/table/cmdb_ci/c6c46ddedb062f0036bc125239961990",
#                                              "value": "c6c46ddedb062f0036bc125239961990"
#                            },
#                            "promoted_by": "",
#                            "impact": "3",
#                            "u_vendor_reference": "",
#                            "knowledge": "false",
#                            "proposed_on": "",
#                            "work_start": "",
#                            "u_accountable": "",
#                            "u_incor_cat_pri": "false",
#                            "due_date": "",
#                            "vendor": "",
#                            "sys_updated_on": "2019-10-28 09:11:14",
#                            "reopen_count": "0",
#                            "service_offering": "",
#                            "closed_by": {
#                                               "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user/3c31197edba4e30036bc1252399619c6",
#                                               "value": "3c31197edba4e30036bc1252399619c6"
#                            },
#                            "u_vulnerability_id": "",
#                            "incident_state": "7",
#                            "reopened_by": "",
#                            "u_noc_wrg_asgmt": "false",
#                            "vendor_closed_at": "",
#                            "work_notes_list": "",
#                            "opened_by": {
#                                               "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user/3c31197edba4e30036bc1252399619c6",
#                                               "value": "3c31197edba4e30036bc1252399619c6"
#                            },
#                            "sys_domain": {
#                                               "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user_group/global",
#                                               "value": "global"
#                            },
#                            "business_stc": "0",
#                            "actions_taken": "",
#                            "escalation": "0",
#                            "sys_tags": "",
#                            "made_sla": "true",
#                            "group_list": "",
#                            "sys_mod_count": "16",
#                            "major_incident_state": "",
#                            "time_worked": "",
#                            "u_current_action": "",
#                            "description": "received from: robrecht.jacobs.ftb@ts.fujitsu.com\n\n Subject: GoLive PROD Email integration test 3\n\nDO NOT HANDLE THIS EMAIL THIS IS A TEST!\n\nGoLive PROD Email integration test 3\n[cid:image004.jpg@01D488C8.D4B9A540]\n\nWith best regards,\n\nRobrecht Jacobs\nJunior ServiceNow Consultant\n\n[cid:image004.gif@01CEE08A.195DCF10]\nFUJITSU\nSquare Marie Curie 12, B-1070 Brussels, Belgium\nMob.: +32 (0) 477 04 51 58\nE-mail: robrecht.jacobs.ftb@ts.fujitsu.com\nWeb: ts.fujitsu.com<http://ts.fujitsu.com/>\nCompany details: Fujitsu Technology Solutions <legal form> / ts.fujitsu.com/imprint<http://ts.fujitsu.com/imprint.html>\nThis communication contains information that is confidential, proprietary in nature and/or privileged.  It is for the exclusive use of the intended recipient(s). If you are not the intended recipient(s) or the person responsible for delivering it to the intended recipient(s), please note that any form of dissemination, distribution or copying of this communication is strictly prohibited and may be unlawful. If you have received this communication in error, please immediately notify the sender and delete the original communication. Thank you for your cooperation.\nPlease be advised that neither Fujitsu, its affiliates, its employees or agents accept liability for any errors, omissions or damages caused by delays of receipt or by any virus infection in this message or its attachments, or which may otherwise arise as a result of this e-mail transmission.",
#                            "category": "network",
#                            "rfc": "",
#                            "u_servicedesk_reference": "",
#                            "sys_updated_by": "yves",
#                            "caused_by": "",
#                            "vendor_point_of_contact": "",
#                            "comments": "",
#                            "activity_due": "",
#                            "state": "7",
#                            "opened_at": "2018-11-30 15:28:49",
#                            "sla_due": "",
#                            "cause": "",
#                            "follow_up": "",
#                            "sys_class_name": "incident",
#                            "parent": "",
#                            "business_duration": "1970-01-01 00:00:00",
#                            "timeline": "",
#                            "work_notes": "",
#                            "active": "false",
#                            "approval": "not requested",
#                            "resolved_at": "2018-11-30 15:49:54",
#                            "work_end": "",
#                            "sys_created_on": "2018-11-30 15:28:49",
#                            "correlation_display": "",
#                            "assignment_group": {
#                                                 "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user_group/4469e5a2db1f9b0022367742399619b0",
#                                                 "value": "4469e5a2db1f9b0022367742399619b0"
#                            },
#                            "business_impact": "",
#                            "u_b_trans": "false",
#                            "subcategory": "bgp_issue",
#                            "business_service": {
#                                                 "link": "https://belnetdev.service-now.com/api/now/v1/table/cmdb_ci_service/fc50dd9edbc22f0036bc1252399619c1",
#                                                 "value": "fc50dd9edbc22f0036bc1252399619c1"
#                            },
#                            "overview": "",
#                            "number": "INC0010174",
#                            "notify": "1",
#                            "resolved_by": {
#                                                "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user/3c31197edba4e30036bc1252399619c6",
#                                                "value": "3c31197edba4e30036bc1252399619c6"
#                            },
#                            "upon_reject": "cancel",
#                            "upon_approval": "proceed",
#                            "expected_start": "",
#                            "calendar_stc": "1265",
#                            "vendor_ticket": "",
#                            "problem_id": "",
#                            "priority": "4",
#                            "sys_id": "8f37de1adbc62f0036bc12523996193c",
#                            "child_incidents": "0",
#                            "location": "",
#                            "lessons_learned": "",
#                            "company": {
#                                                "link": "https://belnetdev.service-now.com/api/now/v1/table/core_company/3f4d05dadbc22f0036bc1252399619c7",
#                                                "value": "3f4d05dadbc22f0036bc1252399619c7"
#                            },
#                            "parent_incident": {
#                                                "link": "https://belnetdev.service-now.com/api/now/v1/table/incident/c179d8c9dbf840502423a5ca0b96193c",
#                                                "value": "c179d8c9dbf840502423a5ca0b96193c"
#                            },
#                            "promoted_on": "",
#                            "u_sd_wrg_asgmt": "false",
#                            "u_re_wrg_lg": "false",
#                            "closed_at": "2018-11-30 15:52:21",
#                            "u_accountable_group": {
#                                                "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user_group/4469e5a2db1f9b0022367742399619b0",
#                                                "value": "4469e5a2db1f9b0022367742399619b0"
#                            },
#                            "reopened_time": "",
#                            "u_on_hold_reason": "",
#                            "user_input": "",
#                            "watch_list": "",
#                            "trigger_rule": "",
#                            "caller_id": {
#                                                "link": "https://belnetdev.service-now.com/api/now/v1/table/sys_user/3c31197edba4e30036bc1252399619c6",
#                                                "value": "3c31197edba4e30036bc1252399619c6"
#                            },
#                            "correlation_id": "",
#                            "severity": "3",
#                            "vendor_resolved_at": "",
#                            "vendor_opened_at": "",
#                            "sys_created_by": "robrecht.jacobs",
#                            "short_description": "GoLive PROD Email integration test 3",
#                            "comments_and_work_notes": "",
#                            "u_jcrtag": "",
#                            "proposed_by": "",
#                            "close_notes": "CLOSED DO NOT HANDLE THIS TICKET",
#                            "u_alarm_raised_time": "",
#                            "reassignment_count": "0",
#                            "approval_history": "",
#                            "calendar_duration": "1970-01-01 00:21:05",
#                            "sys_domain_path": "/",
#                            "hold_reason": "",
#                            "contact_type": "email",
#                            "close_code": "Solved (Permanently)",
#                            "assigned_to": "",
#                            "approval_set": "",
#                            "order": "",
#                            "urgency": "3"
#                }
#       ]
#}
